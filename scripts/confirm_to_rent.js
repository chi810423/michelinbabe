let map;

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 8,
    });
}

app.controller('confirm_to_rentController', function($scope, $window) {
    // default map options
    $('.pageTitle').html("租賃確認單");
    $(".leftFun").click(function() {
        $window.open("#!/reservation", "_self")
    });
    $(".rightFun").click(function() {
        $window.open("home.html", "_self")
    });
    $(".btnPurple").click(function() {
        $window.open("#!/rent_confirm", "_self")
    });


    var mapOptions = {
        center: { lat: 25.0643052, lng: 121.520376 },
        zoom: 8
    };
    $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

    $scope.loadMap = function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var marker = new google.maps.Marker({
                    position: { lat: 25.0643052, lng: 121.520376 },
                    map: $scope.map
                });
                var contentString = '<h2>帝國璽園/24F-3室<h2>';
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                infowindow.open($scope.map, marker);
                $scope.map.setCenter(pos);
            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation

            handleLocationError(false, infoWindow, map.getCenter());
        }
    };

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
    }

});