app.controller('reservationController', function($scope, $window) {
    // default map options
    $('.pageTitle').html("賞屋預約提醒");
    $(".leftFun").click(function() {
        clearTimeout(myVarTimeout);
        $window.open("#!/booking", "_self")
    });
    $(".rightFun").click(function() {
        clearTimeout(myVarTimeout);
        $window.open("home.html", "_self")
    });

    $(".btnWhite").click(function() {
        clearTimeout(myVarTimeout);
        $window.open("#!/booking", "_self")
    });
    $(".cardContainer").click(function() {
        clearTimeout(myVarTimeout);
        $window.open("#!/confirm_to_rent", "_self")
    });
    myVarTimeout = setTimeout(function() {
        $window.open("#!/confirm_to_rent", "_self")
    }, 60000);



});