app.controller('payController', function($scope, $window) {
    // default map options
    $('.pageTitle').html("租賃約期繳款方式");
    $(".leftFun").click(function() {
        $window.open("#!/rent_confirm", "_self")
    });
    $(".rightFun").click(function() {
        $window.open("home.html", "_self")
    });
    $(".btnPurple").click(function() {
        $window.open("#!/pay_done", "_self")
    });

    $(".tabRadio input[type='radio']").click(function() {
        if ($("#tabRadio1").is(":checked")) {
            $("#paymentAccount").addClass("show");
            $("#paymentCredit").removeClass("show");
        } else {
            if ($("#tabRadio2").is(":checked")) {
                $("#paymentAccount").removeClass("show");
                $("#paymentCredit").addClass("show");
            }
        }
    })




});