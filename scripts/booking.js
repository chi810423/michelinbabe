app.controller('bookingController', function($scope, $window) {
    // default map options
    $('.pageTitle').html("預約賞屋");
    $(".leftFun").click(function() {
        $window.open("#!/room_info", "_self")
    });
    $(".rightFun").click(function() {
        $window.open("home.html", "_self")
    });

    $(".btnPurple").click(function() {
        $window.open("#!/reservation", "_self")
    });

    $(".calCell.able").click(function() {
        $(this).addClass("select");
        $(this).siblings(".calCell").removeClass("select");
        $(this).parents(".calRow").siblings(".calRow").children(".calCell").removeClass("select");
    })
    $(".tabRadio input[type='radio']").click(function() {
        if ($("#tabRadio1").is(":checked")) {
            $("#timeMorning").addClass("show");
            $("#timeMorning").siblings(".detailTimeContainer").removeClass("show");
        } else {
            if ($("#tabRadio2").is(":checked")) {
                $("#timeNoon").addClass("show");
                $("#timeNoon").siblings(".detailTimeContainer").removeClass("show");
            } else {
                if ($("#tabRadio3").is(":checked")) {
                    $("#timeNight").addClass("show");
                    $("#timeNight").siblings(".detailTimeContainer").removeClass("show");
                }
            }
        }
    })
    $(".timeCell").click(function() {
        $(this).addClass("select");
        $(this).siblings(".timeCell").removeClass("select");
        $(this).parents(".timeRow").siblings(".timeRow").children(".timeCell").removeClass("select");
    });






});