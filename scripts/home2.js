var app = angular.module("home2App", []);
app.controller('home2Controller', function($scope, $window) {
    // default map options
    const videoElement = document.getElementById('home_video');
    if (videoElement.playing) {
        // video is already playing so do nothing
    } else {
        // video is not playing
        // so play video now
        videoElement.play();
    }


});