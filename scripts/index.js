var app = angular.module("myApp", ["ngRoute"]);
var myVarTimeout;
app.config(function($routeProvider) {
    $routeProvider
    // .when("/", {
    //     templateUrl: "pages/home.html",
    //     controller: 'homeController'
    // })
        .when("/confirm_to_rent", {
            templateUrl: "pages/tenant_a.4_confirm_to_rent.html",
            controller: 'confirm_to_rentController'
        })
        .when("/landlord_object_management", {
            templateUrl: "pages/landlord_object_management.html",
            controller: 'landlord_object_managementController'
        })
        .when("/room_info", {
            templateUrl: "pages/tenant_a.1_room_info.html",
            controller: 'roomInfoController'
        })
        .when("/reservation", {
            templateUrl: "pages/tenant_a.3_reservation.html",
            controller: 'reservationController'
        })
        .when("/pay", {
            templateUrl: "pages/tenant_b.1_pay.html",
            controller: 'payController'
        })
        .when("/pay_done", {
            templateUrl: "pages/tenant_b.2_pay_done.html",
            controller: 'pay_doneController'
        })
        .when("/pay_list", {
            templateUrl: "pages/tenant_b.3_pay_list.html",
            controller: 'pay_listController'
        })
        .when("/booking", {
            templateUrl: "pages/tenant_a.2_booking.html",
            controller: 'bookingController'
        })
        .when("/rent_confirm", {
            templateUrl: "pages/tenant_a.5_rent_confirm.html",
            controller: 'rent_confirmController'
        });
});